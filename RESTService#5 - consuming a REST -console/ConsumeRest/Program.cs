﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModelLib.Model;

namespace ConsumeRest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("lets print out all the items !");
            Worker testWorker = new Worker();
            
            testWorker.Start();

            //Task<IList<Item>> dataList = testWorker.GetAllItemsAsync();
            List<Item> Items = testWorker.GetAllItemsAsync().Result.ToList();

            foreach (Item item in Items)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine();
            Console.WriteLine("cool, lets just get one, with the id of 3");
            Console.WriteLine();

            Console.WriteLine(testWorker.GetOneItemAsync(3).Result.ToString());

            Console.WriteLine();
            Console.WriteLine("lets post a new obj. we only get to pick the name for now");
            
            // Console.WriteLine("write the name of the item");
            //string name = Console.ReadLine();

            //testWorker.PostOneItemAsync(new Item(444, name, "low", 404));

            Console.WriteLine("click enter after 10 sec");
            Console.ReadLine();

            Items = testWorker.GetAllItemsAsync().Result.ToList();

            //foreach (Item item in Items)
            //{
            //    Console.WriteLine(item.ToString());
            //}

            //Console.WriteLine("pick id of one to edit");
            //int id = Int32.Parse(Console.ReadLine());
            //testWorker.PutOneItemAsync(id, new Item(id, "ThisIsTheNewName", "low", 202));

            Console.WriteLine("click enter after 10 sec");
            Console.ReadLine();

            foreach (Item item in Items)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine("lets delete all the newly added items");

            Console.WriteLine("click enter after 10 sec");
            Console.ReadLine();

            //testWorker.DeleteOneItemAsync(444);
            //testWorker.DeleteOneItemAsync(440);
            //testWorker.DeleteOneItemAsync(44);
            //testWorker.DeleteOneItemAsync(45);
            //testWorker.DeleteOneItemAsync(45);



            Console.ReadLine();
        }
    }
}
