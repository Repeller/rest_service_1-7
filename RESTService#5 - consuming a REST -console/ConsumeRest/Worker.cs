﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ModelLib.Model;
using Newtonsoft.Json;

namespace ConsumeRest
{
    class Worker
    {

        public Worker()
        {
            
        }

        public void Start()
        {

        }

        /// <summary>
        /// gets the values from the web api and converts it to item objs
        /// </summary>
        /// <returns>the list converted from json data</returns>
        public async Task<IList<Item>> GetAllItemsAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                string content = await client.GetStringAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems");
                // we take the json data and Deserialize it to objects of type <IList<Item>>
                IList<Item> cList =
                    JsonConvert.DeserializeObject<IList<Item>>(content);
                return cList;
            }
        }

        /// <summary>
        /// returns just one item
        /// </summary>
        /// <param name="id">the id of the item you want</param>
        /// <returns>the item found with that id</returns>
        public async Task<Item> GetOneItemAsync(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                string content = await client.GetStringAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems/" + id);
                // we take the json data and Deserialize it to objects of type <IList<Item>>
                Item test = JsonConvert.DeserializeObject<Item>(content);

                return test;
            }
        }

        /// <summary>
        /// post one item to the web api
        /// </summary>
        /// <param name="value">the item that will be posted to the web api</param>
        /// <returns>the newly made item</returns>
        public async void PostOneItemAsync(Item value)
        {
            using (HttpClient client = new HttpClient())
            {
                string content = await client.GetStringAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems");
                // we take the item obj and convert it to json format, before we upload it

                string ItemAsJson = JsonConvert.SerializeObject(value);
                StringContent postContent = new StringContent(ItemAsJson, Encoding.UTF8, "application/json");

                // here we add it to the web api and store it as "returnItem"
                
                await client.PostAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems", postContent);


                // this is not needed here, but we are doing it anyways
                //Item test = JsonConvert.DeserializeObject<Item>(retunItem.ToString());
                //return test;
            }
        }

        /// <summary>
        /// put new values in to a item
        /// </summary>
        /// <param name="id">the id of the item you want to edit</param>
        /// <param name="value">the new item values</param>
        public async void PutOneItemAsync(int id, Item value)
        {
            using (HttpClient client = new HttpClient())
            {

                // we take the item obj and convert it to json format, before we upload it

                // just to be sure, lets overwrite the id
                value.Id = id;

                string ItemAsJson = JsonConvert.SerializeObject(value);
                StringContent putContent = new StringContent(ItemAsJson, Encoding.UTF8, "application/json");

                // here we add it to the web api and store it as "returnItem"
                
                await client.PutAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems/" + id, putContent);

                // this is not needed here, but we are doing it anyways
                //Item test = JsonConvert.DeserializeObject<Item>(retunItem.ToString());
                //return test;
            }
        }

        /// <summary>
        /// delete one item from the web api
        /// </summary>
        /// <param name="id">the id of the item that you want to delete</param>
        public async void DeleteOneItemAsync(int id)
        {
            using (HttpClient client = new HttpClient())
            {

                //StringContent putContent = new StringContent(ItemAsJson, Encoding.UTF8, "application/json");
                
                await client.DeleteAsync("https://restservice-4-azuretest.azurewebsites.net/api/LocalItems/" + id);
            }
        }
    }
}
