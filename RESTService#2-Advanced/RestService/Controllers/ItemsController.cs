﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ModelLib.Model;

namespace RestService.Controllers
{
    [Route("api/localItems")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private static readonly List<Item> Items = new List<Item>()
        {
            new Item(1, "Bread", "low", 33),
            new Item(2, "bread", "middle", 21),
            new Item(3, "beer", "low", 70.5),
            new Item(4, "soda", "high", 21.4),
            new Item(5, "milk", "low", 55.8)
        };


        // GET: api/Items
        [HttpGet]
        public IEnumerable<Item> Get()
        {
            //return new string[] { "value1", "value2" };
            return Items;
        }

        // GET: api/Items/5
        //[HttpGet("{id}", Name = "Get")]
        [HttpGet]
        [Route("{id}")]
        public Item Get(int id)
        {
            Item error = new Item(0, "error", "error", 404.404);
            
            if (Items.Exists(x => x.Id == id))
            {
                return Items.Find(i => i.Id == id);
            }
            else
            {
                return error;
            }
        }

        // POST: api/Items
        [HttpPost]
        public void Post([FromBody] Item value)
        {
            Items.Add(value);
        }

        // PUT: api/Items/5
        //[HttpPut("{id}")]
        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] Item value)
        {
            Item item = Get(id);
            if (item != null)
            {
                item.Id = value.Id;
                item.Name = value.Name;
                item.Quality = value.Quality;
                item.Quantity = item.Quantity;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            Item item = Get(id);
            if(item != null)
                Items.Remove(item);
        }

        // new custom method
        [HttpGet]
        [Route("Name/{substring}")]
        public IEnumerable<Item> GetFromSubstring(string substring)
        {
            return Items.FindAll(i => i.Name.Contains(substring));
        }

        // new custom method
        [HttpGet]
        [Route("Low/")]
        public IEnumerable<Item> GetAllLow()
        {
            return Items.FindAll(i => i.Quality.Contains("low"));
        }

        // new custom method
        [HttpGet]
        [Route("Middle/")]
        public IEnumerable<Item> GetAllMiddle()
        {
            return Items.FindAll(i => i.Quality.Contains("middle"));
        }

        // new custom method
        [HttpGet]
        [Route("High/")]
        public IEnumerable<Item> GetAllHigh()
        {
            return Items.FindAll(i => i.Quality.Contains("high"));
        }

        [HttpGet]
        [Route("Search")]
        public IEnumerable<Item> GetWithFilter([FromQuery] ModelLib.Model.FilterItem filter)
        {
            bool isLow = !String.IsNullOrEmpty(filter.LowQuantity);
            bool isHigh = !String.IsNullOrEmpty(filter.HighQuantity);

            double lowValue = Convert.ToDouble(filter.LowQuantity);
            double highValue = Convert.ToDouble(filter.HighQuantity);

            // switch between the values, in case of error 40
            if (lowValue > highValue)
            {
                double temp = lowValue;
                lowValue = highValue;
                highValue = temp;
            }

            if (isLow && isHigh == false) // only low
            {
                return Items.FindAll(i => i.Quantity == lowValue);
            }
            else if (isHigh && isLow == false) // only high
            {
                return Items.FindAll(i => (i.Quantity >= 0) && (i.Quantity <= highValue));
            }
            else if (isHigh && isLow) // both low and high
            {
                return Items.FindAll(i => (i.Quantity >= lowValue) && (i.Quantity <= highValue));
            }

            return new List<Item>(); 
        }
    }
}
