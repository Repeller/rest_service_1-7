using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestService.Controllers;
using RestService.Models;
using Item = RestService.Models.Item;

namespace RestTestProject
{
    [TestClass]
    public class UnitTest1
    {
        // check the "get the default 5 items"
        [TestMethod]
        public void CheckGetDefault()
        {
            ItemsController controllerTest = new ItemsController();
            IEnumerable<Item> allItems = controllerTest.Get();

            Assert.AreEqual(allItems.Count(), 5);
        }

        // check the 'get' one by id
        [TestMethod]
        public void CheckGetOne()
        {
            ItemsController controllerTest = new ItemsController();
            Item testItem = controllerTest.Get(1);

            Assert.AreEqual(testItem.Name, "Bread");
        }

        // check the 'post' one and then get it after to check it
        [TestMethod]
        public void CheckPostOne()
        {
            ItemsController controllerTest = new ItemsController();
            controllerTest.Post(new Item(6, "batman", "high", 666));
            Item testItem = controllerTest.Get(6);

            Assert.AreEqual(testItem.Name, "batman");
        }

        // check the 'Delete' one
        [TestMethod]
        public void CheckDeleteOne()
        {
            ItemsController controllerTest = new ItemsController();
            controllerTest.Post(new Item(6, "batman", "high", 666));
            Item testItem = controllerTest.Get(6);
            int before = controllerTest.Get().Count();

            controllerTest.Delete(6);
            int after = controllerTest.Get().Count();

            int expected = 5;
            Assert.AreEqual(after, expected);
        }

        // check the GetAllLow' 
        [TestMethod]
        public void CheckGetAllLows()
        {
            ItemsController controllerTest = new ItemsController();
            int number = controllerTest.GetAllLow().Count();
            int expected = 3;

            Assert.AreEqual(number, expected);
        }

        // check the GetAllLow' 
        [TestMethod]
        public void CheckGetAllMiddle()
        {
            ItemsController controllerTest = new ItemsController();
            int number = controllerTest.GetAllMiddle().Count();
            int expected = 1;

            Assert.AreEqual(number, expected);
        }

        // check the GetAllLow' 
        [TestMethod]
        public void CheckGetAllHigh()
        {
            ItemsController controllerTest = new ItemsController();
            int number = controllerTest.GetAllHigh().Count();
            int expected = 1;

            Assert.AreEqual(number, expected);
        }

        // get with filter

        // get from Sub-String

    }
}
