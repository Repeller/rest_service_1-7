﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RestService.Models;
using RestService.DButil;

namespace RestService.Controllers
{
    [Route("api/localItems")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        //private static readonly List<Item> Items = new List<Item>()
        //{
        //    new Item(1, "Bread", "low", 33),
        //    new Item(2, "bread", "middle", 21),
        //    new Item(3, "beer", "low", 70.5),
        //    new Item(4, "soda", "high", 21.4),
        //    new Item(5, "milk", "low", 55.8)
        //};

        ManageItems manageItems = new ManageItems();

        //ctor
        public ItemsController()
        {
            manageItems.Items = manageItems.loadItems();
        }

        // GET: api/Items

        /// <summary>
        /// gets all the items
        /// </summary>
        /// <returns>the list of all items</returns>
        [HttpGet]
        public IEnumerable<Item> Get()
        {
            //return new string[] { "value1", "value2" };
            return manageItems.Items;
        }

        // GET: api/Items/5
        //[HttpGet("{id}", Name = "Get")]
        /// <summary>
        /// get one item by id
        /// </summary>
        /// <param name="id">the id of the item you want</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}")]
        public Item Get(int id)
        {
            //Item error = new Item(0, "error", "error", 404.404);

            return manageItems.Get(id);
        }

        // POST: api/Items
        /// <summary>
        /// post one item to the list
        /// </summary>
        /// <param name="value">the item you want to add to the list</param>
        [HttpPost]
        public void Post([FromBody] Item value)
        {
            manageItems.Post(value);
        }

        // PUT: api/Items/5
        //[HttpPut("{id}")]
        /// <summary>
        /// put/edit one item's values and replace them with new ones
        /// </summary>
        /// <param name="id">the id of the item to put into</param>
        /// <param name="value">the value that will be put into the item</param>
        [HttpPut]
        [Route("{id}")]
        public void Put(int id, [FromBody] Item value)
        {
            manageItems.Put(id, value);
        }

        // DELETE: api/ApiWithActions/5
        /// <summary>
        /// delete one item from the list
        /// </summary>
        /// <param name="id">the id of the item you want to delete</param>
        [HttpDelete]
        [Route("{id}")]
        public void Delete(int id)
        {
            manageItems.Delete(id);
        }

        // new custom method
        /// <summary>
        /// Get all item that have a specific text/name
        /// </summary>
        /// <param name="substring">the name you want to look for</param>
        /// <returns>the items that was found, that had the name you are looking for</returns>
        [HttpGet]
        [Route("Name/{substring}")]
        public IEnumerable<Item> GetFromSubstring(string substring)
        {
            List<Item> tempList = manageItems.Items.ToList();
            return tempList.FindAll(i => i.Name.Contains(substring));
        }

        // new custom method
        /// <summary>
        /// get all the items that have a 'low' quality
        /// </summary>
        /// <returns>the list of found 'low' items</returns>
        [HttpGet]
        [Route("Low/")]
        public IEnumerable<Item> GetAllLow()
        {
            List<Item> tempList = manageItems.Items.ToList();
            return tempList.FindAll(i => i.Quality.Contains("low"));
        }

        // new custom method
        /// <summary>
        /// get all the items that have a 'middle' quality
        /// </summary>
        /// <returns>the list of found 'middle' items</returns>
        [HttpGet]
        [Route("Middle/")]
        public IEnumerable<Item> GetAllMiddle()
        {
            List<Item> tempList = manageItems.Items.ToList();
            return tempList.FindAll(i => i.Quality.Contains("middle"));
        }

        // new custom method
        /// <summary>
        /// get all the items that have a 'high' quality
        /// </summary>
        /// <returns>the list of found 'high' items</returns>
        [HttpGet]
        [Route("High/")]
        public IEnumerable<Item> GetAllHigh()
        {
            List<Item> tempList = manageItems.Items.ToList();
            return tempList.FindAll(i => i.Quality.Contains("high"));
        }

        /// <summary>
        /// Search for items that are in between 2 quantity values
        /// </summary>
        /// <param name="filter">custom class, that have 2 string props "LowQuantity" and "HighQuantity"</param>
        /// <returns>a list of all the found items, that was in between the 2 quantity values</returns>
        [HttpGet]
        [Route("Search")]
        public IEnumerable<Item> GetWithFilter([FromQuery] Models.FilterItem filter)
        {
            bool isLow = !String.IsNullOrEmpty(filter.LowQuantity);
            bool isHigh = !String.IsNullOrEmpty(filter.HighQuantity);

            double lowValue = Convert.ToDouble(filter.LowQuantity);
            double highValue = Convert.ToDouble(filter.HighQuantity);

            List<Item> tempList = manageItems.Items.ToList();

            // switch between the values, in case of error 40
            if (lowValue > highValue)
            {
                double temp = lowValue;
                lowValue = highValue;
                highValue = temp;
            }

            if (isLow && isHigh == false) // only low
            {
                return tempList.FindAll(i => i.Quantity == lowValue);
            }
            else if (isHigh && isLow == false) // only high
            {
                return tempList.FindAll(i => (i.Quantity >= 0) && (i.Quantity <= highValue));
            }
            else if (isHigh && isLow) // both low and high
            {
                return tempList.FindAll(i => (i.Quantity >= lowValue) && (i.Quantity <= highValue));
            }

            return new List<Item>(); 
        }
    }
}
