﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestService.Models
{
    public class FilterItem
    {
        public string LowQuantity { get; set; }
        public string HighQuantity { get; set; }

        public FilterItem()
        {
            
        }

        public FilterItem(string lowQuantity, string highQuantity)
        {
            LowQuantity = lowQuantity;
            HighQuantity = highQuantity;
        }
    }
}
