﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using RestService.Models;

namespace RestService.DButil
{
    public class ManageItems
    {
        // fields
        private const string _connectionString = "Server=tcp:myfoodjournal-server.database.windows.net,1433;" +
                                                 "Initial Catalog = MyFoodJournalDB;Persist Security Info=False;" +
                                                 "User ID = AdminFood;" +
                                                 "Password=MyFoodApp#1234;" +
                                                 "MultipleActiveResultSets=False;" +
                                                 "Encrypt=True;" +
                                                 "TrustServerCertificate=False;" +
                                                 "Connection Timeout = 30;";
        private const string GET_ALL = "select * from dbo.Item";
        private const string GET_ONE = "select * from dbo.Item WHERE Id = @ID";
        private const string POST_ONE = "insert into dbo.Item (Name, Quality, Quantity) VALUES (@NAME, @QUALITY, @QUANTITY)";

        private const string PUT_ONE =
            "UPDATE dbo.Item SET Name = @NAME, Quality = @QUALITY, Quantity = QUANTITY WHERE Id = @ID";

        private const string DELETE_ONE = "DELETE FROM dbo.Item WHERE Id = @ID";

        // prop
        public IEnumerable<Item> Items { get; set; }

        // ctor
        public ManageItems()
        { 
        }

        // ctorp
        //public ManageItems(IEnumerable<Item> items)
        //{
        //    Items = loadItems();
        //}

        public IEnumerable<Item> Get()
        {
            return Items;
        }

        /// <summary>
        /// reads the data from the reader and returns a item obj
        /// </summary>
        /// <param name="reader">the reader in use</param>
        /// <returns>the item obj from the reader</returns>
        protected Item ReadNextElement(SqlDataReader reader)
        {
            Item item = new Item();

            item.Id = reader.GetInt32(0);
            item.Name = reader.GetString(1);
            item.Quality = reader.GetString(2);
            item.Quantity = reader.GetDouble(3);

            return item;
        }

        /// <summary>
        /// used to load everything into the list
        /// </summary>
        /// <returns>the list of all items</returns>
        public IEnumerable<Item> loadItems()
        {
            List<Item> tempItems = new List<Item>();
            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ALL, Conn))
            {
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Item value = ReadNextElement(reader);
                    tempItems.Add(value);
                }
                reader.Close();
                Conn.Close();
            }

            return tempItems;
        }

        public Item Get(int id)
        {
            Item tempItem = new Item();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(GET_ONE, Conn))
            {
                cmd.Parameters.AddWithValue("@ID", id);
                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    Item value = ReadNextElement(reader);
                    if (value.Id == id)
                    {
                        tempItem = value;
                        break;
                    }
                }
                reader.Close();
                Conn.Close();
            }

            return tempItem;
        }

        public void Post(Item value)
        {
            Item tempItem = new Item();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(POST_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@NAME", value.Name);
                cmd.Parameters.AddWithValue("@QUALITY", value.Quality);
                cmd.Parameters.AddWithValue("@QUANTITY", value.Quantity);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempItem = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Items = loadItems();
            }
        }

        public void Put(int id, Item value)
        {
            Item tempItem = new Item();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);
                cmd.Parameters.AddWithValue("@NAME", value.Name);
                cmd.Parameters.AddWithValue("@QUALITY", value.Quality);
                cmd.Parameters.AddWithValue("@QUANTITY", value.Quantity);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempItem = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Items = loadItems();
            }
        }

        public void Delete(int id)
        {
            Item tempItem = new Item();

            using (SqlConnection Conn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(PUT_ONE, Conn))
            {
                // we will not use the id, since that get created in the DB
                cmd.Parameters.AddWithValue("@ID", id);

                Conn.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tempItem = ReadNextElement(reader);
                }
                reader.Close();
                Conn.Close();

                // TODO : maybe remove this line
                Items = loadItems();
            }
        }
    }
}
