﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ModelLib.Model;

namespace RestService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private static readonly List<Item> Items = new List<Item>()
        {
            new Item(1, "Bread", "low", 33),
            new Item(2, "bread", "middel", 21),
            new Item(3, "beer", "low", 70.5),
            new Item(4, "soda", "high", 21.4),
            new Item(5, "milk", "low", 55.8)
        };


        // GET: api/Items
        [HttpGet]
        public IEnumerable<Item> Get()
        {
            //return new string[] { "value1", "value2" };
            return Items;
        }

        // GET: api/Items/5
        [HttpGet("{id}", Name = "Get")]
        public Item Get(int id)
        {
            Item error = new Item(0, "error", "error", 404.404);
            
            if (Items.Exists(x => x.Id == id))
            {
                return Items.Find(i => i.Id == id);
            }
            else
            {
                return error;
            }
        }

        // POST: api/Items
        [HttpPost]
        public void Post([FromBody] Item value)
        {
            Items.Add(value);
        }

        // PUT: api/Items/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Item value)
        {
            Item item = Get(id);
            if (item != null)
            {
                item.Id = value.Id;
                item.Name = value.Name;
                item.Quality = value.Quality;
                item.Quantity = item.Quantity;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Item item = Get(id);
            if(item != null)
                Items.Remove(item);
        }
    }
}
